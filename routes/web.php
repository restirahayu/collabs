<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




// Route::get('/guru/create', 'TeacherController@create');
// Route::post('/guru', 'TeacherController@store');
// Route::get('/guru', 'TeacherController@index');
// Route::get('/guru/{id}', 'TeacherController@show');
// Route::get('/guru/{id}/edit', 'TeacherController@edit');
// Route::put('/guru/{id}', 'TeacherController@update');
// Route::delete('/guru/{id}', 'TeacherController@destroy');
// Route::get('/guru/point/edit/{id}', 'TeacherController@editpoint');
Route::get('/guru/cetak', 'TeacherController@cetak')->name('guru.cetak');
// route guru 
Route::resource('guru', 'TeacherController');

// Route::get('/jadwal/create', 'JadwalController@create');
// Route::post('/jadwal', 'JadwalController@store');
// Route::get('/jadwal', 'JadwalController@index');
// // Route::get('/jadwal/{id}', 'JadwalController@show');
// Route::get('/jadwal/{id}/edit', 'JadwalController@edit');
// Route::put('/jadwal/{id}', 'JadwalController@update');
// Route::delete('/jadwal/{id}', 'JadwalController@destroy');

Route::resource('jadwal', 'JadwalController');
Route::get('/jadwal/cetak', 'JadwalController@cetak')->name('jadwal.cetak');


Route::resource('siswa', 'SiswaController');

Route::get('/jadwal/show/cetak', 'JadwalController@cetakpoint');

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');


// route utama
Route::get('/dashboard', 'HomeController@index')->name('dashboard');
Route::get('/', 'HomeController@index')->middleware('auth');
