<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jadwal extends Model
{
    protected $table = 'jadwal';
    protected $guarded = [];

    public function jadwal()
    {
        return $this->hasMany('App\Jadwal', 'user_id');
    }

    public function guru()
    {
        return $this->belongsToMany('App\Guru', 'jadwal_guru', 'jadwal_id', 'guru_id');
    }
    public function siswa()
    {
        return $this->belongsToMany('App\Guru', 'jadwal_siswa', 'jadwal_id', 'siswa_id');
    }
}
