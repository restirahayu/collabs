<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use DB;
use App\Jadwal;
use Auth;
use App\Guru;
use App\Siswa;
use PDF;

class JadwalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function cetak()
    {
        $jadwal = Jadwal::all();

        $pdf = PDF::loadview('jadwal.cetak', compact('jadwal'))->setPaper('a4', 'landscape');
        return $pdf->stream('laporan-jadwal-pdf');
    }

    public function create()
    {
        return view('jadwal.create');
    }
    public function store(Request $request)
    {
        //dd($request->all());
        $request->validate([

            'tanggal' => 'required',
            'tempat' => 'required',
            'mapel' => 'required',
            'waktu' => 'required',

        ]);
        $guru_arr = explode(',', $request["guru"]);

        $guru_ids = [];
        foreach ($guru_arr as $nama_lengkap) {
            $guru = Guru::firstOrCreate(['nama_lengkap' => $nama_lengkap]);
            $guru_ids[] = $guru->id;
        }
        $siswa_arr = explode(',', $request["siswa"]);

        $siswa_ids = [];
        foreach ($siswa_arr as $nama_lengkap) {
            $siswa = Siswa::firstOrCreate(['nama_lengkap' => $nama_lengkap]);
            $siswa_ids[] = $siswa->id;
        }

        $jadwal = Jadwal::create([

            "tanggal" => $request["tanggal"],
            "tempat" => $request["tempat"],
            "mapel" => $request["mapel"],
            "waktu" => $request["waktu"],
            "user_id" => Auth::id()

            // "user_id" => Auth::id()
        ]);

        $jadwal->guru()->sync($guru_ids);
        $jadwal->siswa()->sync($siswa_ids);

        // Alert::success('Sukses', 'Jadwal berhasil disimpan!');

        return redirect('/jadwal')->with('success', 'Jadwal berhasil disimpan!');
    }
    public function index()
    {
        // $user = Auth::user();
        //$jadwal = user->jadwal;
        $jadwal = DB::table('jadwal')->get();
        return view('jadwal.index', compact('jadwal'));
    }
    public function show($id)
    {
        //$jadwal = DB::table('jadwal')->where('id', $id)->first();
        $jadwal = Jadwal::find($id);
        return view('jadwal.show', compact('jadwal'));
    }
    public function edit($id)
    {
        $jadwal = DB::table('jadwal')->where('id', $id)->first();
        return view('jadwal.edit', compact('jadwal'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'tanggal' => 'required',
            'tempat' => 'required',
            'mapel' => 'required',
            'waktu' => 'required',
        ]);
        $update = Jadwal::where('id', $id)->update([
            "tanggal" => $request["tanggal"],
            "tempat" => $request["tempat"],
            "mapel" => $request["mapel"],
            "waktu" => $request["waktu"],
        ]);

        return redirect('/jadwal')->with('success', 'Jadwal berhasil di update!');
    }
    public function destroy($id)
    {
        Jadwal::destroy($id);
        return redirect('/jadwal')->with('success', 'Jadwal berhasil di hapus!');
    }
}
