<?php

namespace App\Http\Controllers;

use App\Guru;
use App\Siswa;
use App\Jadwal;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $guru = Guru::all();
        $siswa = Siswa::all();
        $jadwal = Jadwal::all();

        $mapel = Guru::cursor()->filter(function ($mapel) {
            return $mapel->pengampu;
        });
        // dd($mapel);
        return view('dashboard', compact('guru', 'siswa', 'jadwal', 'mapel'));
    }
}
