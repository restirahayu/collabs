<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

use DB;
use App\Guru;
use Auth;
use PDF;



class TeacherController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    // public function editpoint($id)
    // {
    //     // $select = Guru::all();
    //     $select = Guru::find($id);
    //     // $guruid = $guru->modelKeys();
    //     // $select = Guru::pluck('nama_lengkap', 'id');
    //     // dd($select);


    //     return view('guru.editpoint', compact('select'));
    // }
    // public function cetakpoint()
    // {
    //     $guru = Guru::all();

    //     dd($guru);
    //     return view('guru.cetakpoint', compact('guru'));
    // }

    public function create()
    {
        return view('guru.create');
    }

    public function store(Request $request)
    {
        //dd($request->all());
        $request->validate([
            'nip' => 'required|unique:guru',
            'nama_lengkap' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'agama' => 'required',
            'alamat' => 'required',
            'jenis_kelamin' => 'required',
            'pengampu' => 'required',
            'telepon' => 'required|unique:guru',
        ]);

        $guru = Guru::create([
            "nip" => $request["nip"],
            "nama_lengkap" => $request["nama_lengkap"],
            "tempat_lahir" => $request["tempat_lahir"],
            "tanggal_lahir" => $request["tanggal_lahir"],
            "agama" => $request["agama"],
            "alamat" => $request["alamat"],
            "jenis_kelamin" => $request["jenis_kelamin"],
            "pengampu" => $request["pengampu"],
            "telepon" => $request["telepon"],
            "user_id" => Auth::id()
        ]);
        //Alert::success('Sukses', 'Data berhasil disimpan!');

        return redirect('/guru')->with('success', 'Jadwal berhasil disimpan!');
    }
    public function index()
    {
        $guru = Guru::all();
        // $gurucount = Guru::where('id', $id);

        return view('guru.index', compact('guru'));
    }

    public function show($id)
    {
        // $guru = DB::table('guru')->where('id', $id)->first();
        $guru = Guru::find($id);
        // dd($guru);
        return view('guru.show', compact('guru'));
    }

    public function edit($id)
    {
        // $guru = DB::table('guru')->where('id', $id)->first();
        $guru = Guru::find($id);
        // dd($guru);

        return view('guru.edit', compact('guru'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nip' => 'required',
            'nama_lengkap' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'agama' => 'required',
            'alamat' => 'required',
            'jenis_kelamin' => 'required',
            'pengampu' => 'required',
            'telepon' => 'required',
        ]);
        $update = Guru::where('id', $id)->update([
            "nip" => $request["nip"],
            "nama_lengkap" => $request["nama_lengkap"],
            "tempat_lahir" => $request["tempat_lahir"],
            "tanggal_lahir" => $request["tanggal_lahir"],
            "agama" => $request["agama"],
            "alamat" => $request["alamat"],
            "jenis_kelamin" => $request["jenis_kelamin"],
            "pengampu" => $request["pengampu"],
            "telepon" => $request["telepon"]
        ]);

        return redirect('/guru')->with('success', 'Identitas berhasil di update!');
    }

    public function destroy($id)
    {
        Guru::destroy($id);
        return redirect('/guru')->with('success', 'Identitas berhasil di hapus!');
    }

    public function cetak()
    {
        $guru = Guru::all();
        $pdf = PDF::loadView('guru.cetakdataguru', compact('guru'))->setPaper('a4', 'landscape');
        // dd($pdf);
        return $pdf->stream('Data-Guru.pdf');
    }
}
