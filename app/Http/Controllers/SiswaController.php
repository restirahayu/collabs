<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Siswa;
use Auth;

class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $siswa = Siswa::all();
        return view('siswa.index', compact('siswa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('siswa.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nisn' => 'required|unique:siswa',
            'nama_lengkap' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'agama' => 'required',
            'alamat' => 'required',
            'jenis_kelamin' => 'required',
            'rombel' => 'required',
            'telepon' => 'required|unique:siswa',
        ]);

        $siswa = Siswa::create([
            "nisn" => $request["nisn"],
            "nama_lengkap" => $request["nama_lengkap"],
            "tempat_lahir" => $request["tempat_lahir"],
            "tanggal_lahir" => $request["tanggal_lahir"],
            "agama" => $request["agama"],
            "alamat" => $request["alamat"],
            "jenis_kelamin" => $request["jenis_kelamin"],
            "rombel" => $request["rombel"],
            "telepon" => $request["telepon"],
            "user_id" => Auth::id()
        ]);
        //Alert::success('Sukses', 'Data berhasil disimpan!');

        return redirect('/siswa')->with('success', 'Data berhasil disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $siswa = Siswa::find($id);
        // dd($guru);
        return view('siswa.show', compact('siswa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $siswa = Siswa::find($id);
        // dd($guru);

        return view('siswa.edit', compact('siswa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nisn' => 'required',
            'nama_lengkap' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'agama' => 'required',
            'alamat' => 'required',
            'jenis_kelamin' => 'required',
            'rombel' => 'required',
            'telepon' => 'required',
        ]);
        $update = Siswa::where('id', $id)->update([
            "nisn" => $request["nisn"],
            "nama_lengkap" => $request["nama_lengkap"],
            "tempat_lahir" => $request["tempat_lahir"],
            "tanggal_lahir" => $request["tanggal_lahir"],
            "agama" => $request["agama"],
            "alamat" => $request["alamat"],
            "jenis_kelamin" => $request["jenis_kelamin"],
            "rombel" => $request["rombel"],
            "telepon" => $request["telepon"]
        ]);

        return redirect('/siswa')->with('success', 'Identitas berhasil di update!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Siswa::destroy($id);
        return redirect('/siswa')->with('success', 'Identitas berhasil di hapus!');
    }
}
