@extends('adminlte.master')

@section('content')

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <div class="sparkline13-list">
    <div class="sparkline13-hd">
      <div class="main-sparkline13-hd">
        <h1>Tabel <span class="table-project-n">Jadwal</span> Pelajaran</h1>
      </div>
    </div>
    <div class="card">
      <div class="card-header">
        <h3 class="card-title"></h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        @if(session('success'))
        <div class="alert alert-success">
          {{session('success')}}
        </div>

        @endif

        <a class="btn btn-primary mb-3 btn-sm" href="/jadwal/create"><i class="fas fa-plus-square"></i></a>
        <a class="btn btn-primary mb-3 btn-sm" href="/jadwal/cetak"><i class="fas fa-print"></i></a>
        <table class="table table-bordered">
          <thead>
            <tr>
              <th style="width: 10px">No</th>
              <th>Tanggal</th>
              <th>Tempat</th>
              <th>Mata Pelajaran</th>
              <th>Waktu</th>
              <th>Aksi</th>

              <!-- <th style="width: 40px">Label</th> -->
            </tr>
          </thead>
          <tbody>
            @forelse($jadwal as $key => $jadwal)
            <tr>
              <td>{{ $key+1 }}</td>
              <td>{{ $jadwal->tanggal}}</td>
              <td>{{ $jadwal->tempat}}</td>
              <td>{{ $jadwal->mapel}}</td>
              <!-- <td>{{ $jadwal->mapel}}</td> -->
              <td>{{ $jadwal->waktu}}</td>

              <td style="display: flex;">
                <a href="/jadwal/{{$jadwal->id}}" class="btn btn-info btn-xs"><i class="fas fa-eye"></i></a>
                <a href="/jadwal/{{$jadwal->id}}/edit" class="btn btn-default btn-xs"><i class="fas fa-edit"></i></a>
                <form action="/jadwal/{{$jadwal->id}}" method="post">
                  @csrf
                  @method('DELETE')
                  <input type="submit" value="Hapus" class="btn btn-danger btn-xs">
                </form>
              </td>
            </tr>
            @empty
            <tr>
              <td colspan="11" align="center"> No Post</td>
            </tr>
            @endforelse
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
      <div class="card-footer clearfix">
        <ul class="pagination pagination-sm m-0 float-right">
          <li class="page-item"><a class="page-link" href="#">«</a></li>
          <li class="page-item"><a class="page-link" href="#">1</a></li>
          <li class="page-item"><a class="page-link" href="#">2</a></li>
          <li class="page-item"><a class="page-link" href="#">3</a></li>
          <li class="page-item"><a class="page-link" href="#">»</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>

@endsection