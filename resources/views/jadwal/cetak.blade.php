<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <div class="sparkline13-list">
    <div class="sparkline13-hd">
      <div class="main-sparkline13-hd">
        <h1>Tabel <span class="table-project-n">Jadwal</span> Pelajaran</h1>
      </div>
    </div>
    <div class="card">
      <div class="card-header">
        <h3 class="card-title"></h3>
      </div>
      <!-- /.card-header -->


      <table class="table table-bordered">
        <thead>
          <tr>
            <th style="width: 10px">No</th>
            <th>Tanggal</th>
            <th>Tempat</th>
            <th>Mata Pelajaran</th>
            <th>Waktu</th>

            <!-- <th style="width: 40px">Label</th> -->
          </tr>
        </thead>
        <tbody>
          @foreach($jadwal as $key => $jad)
          <tr>
            <td>{{ $key+1 }}</td>
            <td>{{ $jad->tanggal}}</td>
            <td>{{ $jad->tempat}}</td>
            <td>{{ $jad->mapel}}</td>
            <td>{{ $jad->waktu}}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>