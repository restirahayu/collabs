@extends ('adminlte.master')

@section('content')
<!-- <div class="container"> -->
<div class="position-absolute">
    <h2>Edit Jadwal Mata Pelajaran</h2>
    <!-- <div class="col-5"> -->

    <div class="input-group align-middle">

        <select class="form-select" id="inputGroupSelect04" aria-label="Example select with button addon">
            <option selected>Pilih jadwal yang akan diubah</option>
            @forelse($jadwal as $key => $jadwal)
            <option value="{{$jadwal->id}}">{{ $jadwal->mapel}}</option>
            @empty
            <p>No Data</p>
            @endforelse
        </select>
        <a href="/jadwal/{{$jadwal->id}}/edit">

            <button class="btn btn-outline-secondary" type="submit">Edit</button>
        </a>
    </div>

    <!-- </div> -->

</div>

@endsection