@extends ('adminlte.master')

@section('content')
<div class="col-md-auto mt-2">
    <!-- general form elements disabled -->
    <div class="card card-warning">
        <div class="card-header">
            <h3 class="card-title">Edit mata pelajaran {{$jadwal->mapel}}</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <form action="/jadwal/{{$jadwal->id}}" role="form" method="POST">
                @csrf
                @method('PUT')
                <!-- <div class="row"> -->
                <div class="form-group">
                    <label class="col-form-label" for="inputSuccess"></i>TANGGAL</label>
                    <input name="tanggal" for="tanggal" id="tanggal" type="text" class="form-control" value="{{old('tanggal',$jadwal->tanggal)}}" placeholder="Tanggal">
                    @error('tanggal')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="col-form-label" for="inputSuccess"></i>TEMPAT</label>
                    <input name="tempat" for="tempat" id="tempat" type="text" class="form-control" value="{{old('tempat',$jadwal->tempat)}}" placeholder="Tempat">
                    @error('tempat')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="col-form-label" for="inputSuccess"></i>MATA PELAJARAN</label>
                    <input name="mapel" for="mapel" id="mapel" type="text" class="form-control" value="{{old('mapel',$jadwal->mapel)}}" placeholder="Mata pelajaran">
                    @error('mapel')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="col-form-label" for="inputSuccess"></i>WAKTU</label>
                    <input name="waktu" for="waktu" id="waktu" type="text" class="form-control" value="{{old('waktu',$jadwal->waktu)}}" placeholder="Waktu">
                    @error('waktu')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                </div>
               



                <!-- </div> -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="payment-adress">
                            <button type="submit" class="btn btn-primary waves-effect waves-light">Edit</button>
                            <a href="/guru/show" class="btn btn-primary">Kembali</a>
                        </div>
                    </div>
                </div>

                <!-- input states -->
            </form>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>

@endsection