<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}" defer></script>

  <!-- Fonts -->
  <link rel="dns-prefetch" href="//fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <!-- favicon
		============================================ -->
  <link rel="shortcut icon" type="image/x-icon" href="{{ asset('/adminlte/img/favicon.ico') }}">
  <!-- Google Fonts
		============================================ -->
  <link href="https://fonts.googleapis.com/css?family=Play:400,700" rel="stylesheet">
  <!-- Bootstrap CSS
		============================================ -->
  <link rel="stylesheet" href="{{ asset('/adminlte/css/bootstrap.min.css') }}">
  <!-- Bootstrap CSS
		============================================ -->
  <link rel="stylesheet" href="{{ asset('/adminlte/css/font-awesome.min.css') }}">
  <!-- owl.carousel CSS
		============================================ -->
  <link rel="stylesheet" href="{{ asset('/adminlte/css/owl.carousel.css') }}">
  <link rel="stylesheet" href="{{ asset('/adminlte/css/owl.theme.css') }}">
  <link rel="stylesheet" href="{{ asset('/adminlte/css/owl.transitions.css') }}">
  <!-- animate CSS
		============================================ -->
  <link rel="stylesheet" href="{{ asset('/adminlte/css/animate.css') }}">
  <!-- normalize CSS
		============================================ -->
  <link rel="stylesheet" href="{{ asset('/adminlte/css/normalize.css') }}">
  <!-- main CSS
		============================================ -->
  <link rel="stylesheet" href="{{ asset('/adminlte/css/main.css') }}">
  <!-- morrisjs CSS
		============================================ -->
  <link rel="stylesheet" href="{{ asset('/adminlte/css/morrisjs/morris.css') }}">
  <!-- mCustomScrollbar CSS
		============================================ -->
  <link rel="stylesheet" href="{{ asset('/adminlte/css/scrollbar/jquery.mCustomScrollbar.min.css') }}">
  <!-- metisMenu CSS
		============================================ -->
  <link rel="stylesheet" href="{{ asset('/adminlte/css/metisMenu/metisMenu.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/adminlte/css/metisMenu/metisMenu-vertical.css') }}">
  <!-- calendar CSS
		============================================ -->
  <link rel="stylesheet" href="{{ asset('/adminlte/css/calendar/fullcalendar.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/adminlte/css/calendar/fullcalendar.print.min.css') }}">
  <!-- forms CSS
		============================================ -->
  <link rel="stylesheet" href="{{ asset('/adminlte/css/form/all-type-forms.css') }}">
  <!-- style CSS
		============================================ -->
  <link rel="stylesheet" href="{{ asset('/adminlte/style.css') }}">
  <!-- responsive CSS
		============================================ -->
  <link rel="stylesheet" href="{{ asset('/adminlte/css/responsive.css') }}">
  <!-- modernizr JS
		============================================ -->
  <script src="{{ asset('/adminlte/js/vendor/modernizr-2.8.3.min.js') }}"></script>

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('/adminlte/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{ asset('/adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('/adminlte/dist/css/adminlte.min.css') }}">
</head>

<body>
  <main class="py-4">
    @yield('content')
  </main>
  <!-- </div> -->
  <!-- jquery
		============================================ -->
  <script src="{{ asset('/js/vendor/jquery-1.12.4.min.js') }}"></script>
  <!-- bootstrap JS
		============================================ -->
  <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
  <!-- wow JS
		============================================ -->
  <script src="{{ asset('/js/wow.min.js') }}"></script>
  <!-- price-slider JS
		============================================ -->
  <script src="{{ asset('/js/jquery-price-slider.js') }}"></script>
  <!-- meanmenu JS
		============================================ -->
  <script src="{{ asset('/js/jquery.meanmenu.js') }}"></script>
  <!-- owl.carousel JS
		============================================ -->
  <script src="{{ asset('/js/owl.carousel.min.js') }}"></script>
  <!-- sticky JS
		============================================ -->
  <script src="{{ asset('/js/jquery.sticky.js') }}"></script>
  <!-- scrollUp JS
		============================================ -->
  <script src="{{ asset('/js/jquery.scrollUp.min.js') }}"></script>
  <!-- mCustomScrollbar JS
		============================================ -->
  <script src="{{ asset('/js/scrollbar/jquery.mCustomScrollbar.concat.min.js') }}"></script>
  <script src="{{ asset('/js/scrollbar/mCustomScrollbar-active.js') }}"></script>
  <!-- metisMenu JS
		============================================ -->
  <script src="{{ asset('/js/metisMenu/metisMenu.min.js') }}"></script>
  <script src="{{ asset('/js/metisMenu/metisMenu-active.js') }}"></script>
  <!-- tab JS
		============================================ -->
  <script src="{{ asset('/js/tab.js') }}"></script>
  <!-- icheck JS
		============================================ -->
  <script src="{{ asset('/js/icheck/icheck.min.js') }}"></script>
  <script src="{{ asset('/js/icheck/icheck-active.js') }}"></script>
  <!-- plugins JS
		============================================ -->
  <script src="{{ asset('/js/plugins.js') }}"></script>
  <!-- main JS
		============================================ -->
  <script src="{{ asset('/js/main.js') }}"></script>
  <!-- tawk chat JS
		============================================ -->
  <script src="{{ asset('/js/tawk-chat.js') }}"></script>
  @stack('script')
</body>

</html>