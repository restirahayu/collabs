@extends('adminlte.master')

@section('content')

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <div class="sparkline13-list">
    <div class="sparkline13-hd">
      <div class="main-sparkline13-hd">
        <h1>Tabel <span class="table-project-n">Daftar</span> Siswa</h1>
      </div>
    </div>
    <div class="card">
      <div class="card-header">
        <h3 class="card-title"></h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        @if(session('success'))
        <div class="alert alert-success">
          {{ session('success') }}
        </div>
        @endif

        <a class="btn btn-primary mb-3 btn-sm" href="/siswa/create"><i class="fas fa-plus-square"></a>
        <a class="btn btn-primary mb-3 btn-sm" href="/siswa/cetak"><i class="fas fa-print"></a>
        <table class="table table-bordered">
          <thead>
            <tr>
              <th style="width: 10px">No</th>
              <th>NISN</th>
              <th>Nama Lengkap</th>
              <th>Tempat lahir</th>
              <th>Tanggal lahir</th>
              <th>Agama</th>
              <th>Alamat</th>
              <th>Jenis Kelamin</th>
              <th>Rombel</th>
              <th>Telepon</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            @forelse($siswa as $key => $siswa)
            <tr>
              <td>{{ $key+1 }}</td>
              <td>{{ $siswa->nisn }}</td>
              <td>{{ $siswa->nama_lengkap }}</td>
              <td>{{ $siswa->tempat_lahir }}</td>
              <td>{{ $siswa->tanggal_lahir }}</td>
              <td>{{ $siswa->agama  }}</td>
              <td>{{ $siswa->alamat }}</td>
              <td>{{ $siswa->jenis_kelamin }}</td>
              <td>{{ $siswa->rombel }}</td>
              <td>{{ $siswa->telepon }}</td>
              <td style="display: flex;">
                <a href="/siswa/{{ $siswa->id }}" class="btn btn-info btn-xs"><i class="fas fa-eye"></i></a>
                <a href="/siswa/{{ $siswa->id }}/edit" class="btn btn-default btn-xs"><i class="fas fa-edit"></i></a>
                <form action="/siswa/{{ $siswa->id }}" method="post">
                  @csrf
                  @method('DELETE')
                  <input type="submit" value="Hapus" class="btn btn-danger btn-xs">
                </form>
              </td>
            </tr>
            @empty
            <tr>
              <td colspan="11" align="center"> No Post</td>
            </tr>
            @endforelse
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
      <div class="card-footer clearfix">
        <ul class="pagination pagination-sm m-0 float-right">
          <li class="page-item"><a class="page-link" href="#">«</a></li>
          <li class="page-item"><a class="page-link" href="#">1</a></li>
          <li class="page-item"><a class="page-link" href="#">2</a></li>
          <li class="page-item"><a class="page-link" href="#">3</a></li>
          <li class="page-item"><a class="page-link" href="#">»</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>

@endsection