<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="sparkline13-list">
        <div class="sparkline13-hd">
            <center>

                <div class="main-sparkline13-hd">
                    <h1>Tabel <span class="table-project-n">Daftar</span>Siswa</h1>
                </div>
            </center>
        </div>
        <div class="card">
            <div class="card-header">
                <h3 class="card-title"></h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <!-- <a class="btn btn-primary mb-5" href="/guru/create">Buat Daftar Guru</a> -->
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width: 10px">No</th>
                            <th>NIP</th>
                            <th>Nama Lengkap</th>
                            <th>Tempat lahir</th>
                            <th>Tanggal lahir</th>
                            <th>Agama</th>
                            <th>Alamat</th>
                            <th>Jenis Kelamin</th>
                            <th>Pengampu</th>
                            <th>Telepon</th>
                            <!-- <th>Aksi</th> -->
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($siswa as $key => $siswa)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $siswa->nisn }}</td>
                            <td>{{ $siswa->nama_lengkap }}</td>
                            <td>{{ $siswa->tempat_lahir }}</td>
                            <td>{{ $siswa->tanggal_lahir }}</td>
                            <td>{{ $siswa->agama }}</td>
                            <td>{{ $siswa->alamat }}</td>
                            <td>{{ $siswa->jenis_kelamin }}</td>
                            <td>{{ $siswa->rombel }}</td>
                            <td>{{ $siswa->telepon }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>