@extends ('adminlte.master')

@section('content')
<div class="position-absolute">
    <h2>Edit Data Guru</h2>


    <div class="input-group align-middle">

        <select name="editpoint" id="editpoint" class="form-select" id="inputGroupSelect04">
            <option holder value="">Pilih Data Yang Akan Diedit</option>
            @forelse ($select as $s)
            <option value="{{ $s->id }}" {{ old('editpoint', $s->id) == $s->id ? 'selected' : '' }}> {{ $s->nama_lengkap }}</option>
            @empty
            <p>No Data</p>
            @endforelse
        </select>
        <a href="{{route('guru.edit',['guru' => $s->id])}}" class="btn btn-primary">Edit</a>

    </div>

</div>

@endsection