@extends('adminlte.master')

@section('content')

<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
    <div class="profile-info-inner">
        <div class="profile-img">
            <img src="{{asset('/adminlte/img/profile/1.jpg')}}" alt="">
        </div>
        <div class="profile-details-hr">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                    <div class="address-hr">
                        <p><b>Nama Lengkap</b><br> {{$siswa->nama_lengkap}}</p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                    <div class="address-hr tb-sm-res-d-n dps-tb-ntn">
                        <p><b>Tempat tanggal lahir</b><br> {{$siswa->tempat_lahir}}, {{$siswa->tanggal_lahir}}</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                    <div class="address-hr">
                        <p><b>Agama</b><br>{{$siswa->agama}} </p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                    <div class="address-hr tb-sm-res-d-n dps-tb-ntn">
                        <p><b>Rombel</b><br> {{$siswa->rombel}}</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                    <div class="address-hr">
                        <p><b>Alamat</b><br>{{$siswa->alamat}} </p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                    <div class="address-hr tb-sm-res-d-n dps-tb-ntn">
                        <p><b>Telepon</b><br> {{$siswa->telepon}}</p>
                    </div>
                </div>
            </div>
            <div>
                <a href="/siswa" class="btn btn-primary">Kembali</a>
            </div>
            <!-- <div class="row">
                                    <div class="col-lg-12">
                                        <div class="address-hr">
                                            <p><b>Address</b><br> E104, catn-2, Chandlodia Ahmedabad Gujarat, UK.</p>
                                        </div>
                                    </div>
                                </div> -->
            <!-- <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <div class="address-hr">
                                            <a href="#"><i class="fa fa-facebook"></i></a>
                                            <h3>500</h3>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <div class="address-hr">
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                            <h3>900</h3>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <div class="address-hr">
                                            <a href="#"><i class="fa fa-google-plus"></i></a>
                                            <h3>600</h3>
                                        </div>
                                    </div>
                                </div> -->
        </div>
    </div>
</div>

@endsection