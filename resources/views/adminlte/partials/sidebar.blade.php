<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/dashboard" class="brand-link">
        <img src="{{asset('/adminlte/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{asset('/adminlte/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="/dashboard" class="d-block">{{Auth::user()->name}}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                <li class="nav-item has-treeview">
                    <a href="/dashboard" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                            <!-- <i class="right fas fa-angle-left"></i> -->
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="/guru" class="nav-link">
                        <i class="nav-icon fas fa-copy"></i>
                        <p>
                            Guru
                            <!-- <i class="fas fa-angle-left right"></i> -->
                            <!-- <span class="badge badge-info right">6</span> -->
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="/siswa" class="nav-link">
                        <i class="nav-icon fas fa-chart-pie"></i>
                        <p>
                            Siswa
                            <!-- <i class="right fas fa-angle-left"></i> -->
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="/jadwal" class="nav-link">
                        <i class="nav-icon fas fa-tree"></i>
                        <p>
                            Jadwal
                            <!-- <i class="fas fa-angle-left right"></i> -->
                        </p>
                    </a>
                    <!-- <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="/jadwal" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Daftar Jadwal</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/jadwal/create" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Tambah Jadwal</p>
                            </a>
                        </li>
                         <li class="nav-item">
                            <a href="/jadwal/editpoint" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Edit Jadwal</p>
                            </a>
                        </li> 
                        <li class="nav-item">
                            <a href="/jadwal/show/cetak" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Cetak Jadwal</p>
                            </a>
                    </ul> -->
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>