@extends ('adminlte.master')

@section('content')
<div class="col-md-auto mt-2">
    <!-- general form elements disabled -->
    <div class="card card-warning">
        <div class="card-header">
            <h3 class="card-title">Tambah Data Guru</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <form action="/guru" role="form" method="POST">
                @csrf
                <!-- <div class="row"> -->
                <div class="form-group">
                    <label class="col-form-label" for="inputSuccess"></i>NIP</label>
                    <input name="nip" for="nip" id="nip" type="text" class="form-control" value="{{old('nip','')}}" placeholder="NIP">
                    @error('nip')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                </div>



                <div class="form-group">
                    <label class="col-form-label" for="inputSuccess"></i>NAMA LENGKAP</label>
                    <input name="nama_lengkap" for="nama_lengkap" id="nama_lengkap" type="text" class="form-control" value="{{old('nama_lengkap','')}}" placeholder="Nama lengkap">
                    @error('nama_lengkap')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                </div>


                <div class="form-group">
                    <label class="col-form-label" for="inputSuccess"></i>TEMPAT LAHIR</label>
                    <input name="tempat_lahir" for="tempat_lahir" id="tempat_lahir" type="text" class="form-control" value="{{old('tempat_lahir','')}}" placeholder="Tempat lahir">
                    @error('tempat_lahir')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label class="col-form-label" for="inputSuccess"></i>TANGGAL LAHIR</label>
                    <input name="tanggal_lahir" for="tanggal_lahir" id="tanggal_lahir" type="date" class="form-control" value="{{old('tanggal_lahir','')}}" placeholder="Tanggal lahir">
                    @error('tanggal_lahir')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label class="col-form-label" for="inputSuccess"></i>AGAMA</label>
                    <input name="agama" for="agama" id="agama" type="text" class="form-control" value="{{old('agama','')}}" placeholder="Agama">
                    @error('agama')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label class="col-form-label" for="inputSuccess"></i>ALAMAT</label>
                    <textarea name="alamat" for="alamat" id="alamat" type="text" class="form-control" value="{{old('alamat','')}}" placeholder="Alamat">{{old('alamat','')}}</textarea>
                    @error('alamat')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label class="col-form-label" for="inputSuccess"></i>JENIS KELAMIN</label>
                    <select name="jenis_kelamin" for="jenis_kelamin" id="jenis_kelamin" value="{{old('jenis_kelamin','')}}" class="form-control">
                        <option value="none" selected="" disabled="">Jenis kelamin</option>
                        <option value="Laki-Laki">Laki-laki</option>
                        <option value="Perempuan">Perempuan</option>
                    </select>
                    @error('jenis_kelamin')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label class="col-form-label" for="inputSuccess">PENGAMPU</i></label>
                    <input name="pengampu" for="pengampu" id="pengampu" type="text" class="form-control" value="{{old('pengampu','')}}" placeholder="pengampu">
                    @error('pengampu')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label class="col-form-label" for="inputSuccess">TELEPON</i></label>
                    <input name="telepon" for="telepon" id="telepon" type="number" class="form-control" value="{{old('telepon','')}}" placeholder="No telepon">
                    @error('telepon')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                </div>



                <!-- </div> -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="payment-adress">
                            <button type="submit" class="btn btn-primary waves-effect waves-light">Buat</button>
                        </div>
                    </div>
                </div>

                <!-- input states -->
            </form>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>

@endsection